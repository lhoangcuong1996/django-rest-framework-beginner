from django.urls import path

from .views import (
    ArticleAPIView,
    ArticleDetailAPIView,
    GenericAPIView,
    ListAllView,
    RetrieveUpdateDestroyView,
    article_detail,
    article_list,
    ListAllAuthorView,
    ListAllBookView,
    RetrieveUpdateDestroyAuthorView,
    RetrieveUpdateDestroyBookView
)

urlpatterns = [
    # function API view
    # path('article/', article_list),
    # path('article/detail/<int:pk>', article_detail)
    # class API view
    path("article/", ArticleAPIView.as_view()),
    path("article/<int:id>", ArticleDetailAPIView.as_view()),
    # generic view
    path("generic_article/<int:id>", GenericAPIView.as_view()),
    # shortcut generic view
    path("shortcut_generic_article/", ListAllView.as_view()),
    path("shortcut_generic_article/<int:id>", RetrieveUpdateDestroyView.as_view()),

    path("book/",ListAllBookView.as_view()),
    path("book/<int:id>", RetrieveUpdateDestroyBookView.as_view()),

    path("author/",ListAllAuthorView.as_view()),
    path("author/<int:id>", RetrieveUpdateDestroyAuthorView.as_view())
]
