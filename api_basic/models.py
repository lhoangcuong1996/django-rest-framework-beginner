from django.db import models


# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=1000)
    author = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title

class Book(models.Model):
    title                = models.CharField(max_length=1000)
    description          = models.CharField(max_length=400)
    publisher            = models.CharField(max_length=400)
    author               = models.ManyToManyField('Author',related_name='books', blank=True)

    def __str__(self):
            return self.title


class Author(models.Model):
    name            = models.CharField(max_length=225)
    biography       = models.TextField(blank=True)
    date_of_birth   = models.DateField(blank=True)
    def __str__(self):
        return self.name

# class Order(models.Model):
#     price           = models.IntegerField(max_length=3)
#     date            = models.DateTimeField(blank=True)
#     books           = models.ForeignKey('Book',  blank=True , on_delete=models.DO_NOTHING,)

#     def __str__(self):
#         return self.id