# from django.contrib.auth.models import User
from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, mixins, status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Article,Book, Author
from .serializers import ArticleSerializers, BookSerializers, AuthorSerializers 

# case 1 : use HttpRequest , HttpResponse ... django
# @csrf_exempt
# def article_list(request):
#     if request.method == 'GET':
#         articles = Article.objects.all()
#         articles_serializers = ArticleSerializers(articles, many=True)
#         return JsonResponse(articles_serializers.data, safe=False)
#
#     if request.method == 'POST':
#         data = JSONParser().parse(request)
#         articles_serializers = ArticleSerializers(data=data)
#
#         if articles_serializers.is_valid():
#             articles_serializers.save()
#             return JsonResponse(articles_serializers.data, status=201)
#         return JsonResponse(articles_serializers.errors, status=400)
#
#
# @csrf_exempt
# def article_detail(request, pk):
#     try:
#         article = Article.objects.get(pk=pk)
#
#     except Article.DoesNotExist:
#         return HttpResponse(status=404)
#
#     if (request.method == 'GET'):
#         article_serializers = ArticleSerializers(article)
#         return JsonResponse(article_serializers.data)
#
#     if (request.method == 'PUT'):
#         article_serializers = ArticleSerializers(article)
#         if (article_serializers.is_valid()):
#             article_serializers.save()
#             return JsonResponse(article_serializers.errors, status=400)
#         return JsonResponse(article_serializers.data, status=201)
#     if (request.method == 'DELETE'):
#         article.delete()
#         return HttpResponse(status=204)

# case2 use ApiView RestFramework
@api_view(["GET", "POST"])
def article_list(request):
    if request.method == "GET":
        articles = Article.objects.all()
        articles_serializers = ArticleSerializers(articles, many=True)
        return Response(articles_serializers.data)

    if request.method == "POST":
        data = JSONParser().parse(request.data)
        articles_serializers = ArticleSerializers(data=data)

        if articles_serializers.is_valid():
            articles_serializers.save()
            return Response(articles_serializers.data, status=status.HTTP_201_CREATED)
        return Response(articles_serializers.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET", "PUT", "DELETE"])
def article_detail(request, pk):
    try:
        article = Article.objects.get(pk=pk)

    except Article.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        article_serializers = ArticleSerializers(article)
        return Response(article_serializers.data)

    if request.method == "PUT":
        article_serializers = ArticleSerializers(article, data=request.data)
        if article_serializers.is_valid():
            article_serializers.save()
            return Response(article_serializers.errors)
        return Response(article_serializers.data, status=status.HTTP_404_NOT_FOUND)
    if request.method == "DELETE":
        article.delete()
        return Response(status=status.HTTP_404_NOT_FOUND)


# use API class view instead function view
class ArticleAPIView(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request):
        # abc = settings.AUTH_USER_MODEL.objects.all()
        articles = Article.objects.all()
        articles_serializers = ArticleSerializers(articles, many=True)
        return Response(articles_serializers.data)

    def post(self, request):
        print(request)
        articles_serializers = ArticleSerializers(data=request.data)

        if articles_serializers.is_valid():
            articles_serializers.save()
            return Response(articles_serializers.data, status=status.HTTP_201_CREATED)
        return Response(articles_serializers.errors, status=status.HTTP_400_BAD_REQUEST)


class ArticleDetailAPIView(APIView):
    def get_object(self, id):
        try:
            return Article.objects.get(id=id)
        except Article.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id):
        article = self.get_object(id)
        article_serializer = ArticleSerializers(article)
        return Response(article_serializer.data)

    def put(self, request, id):
        article = self.get_object(id)
        article_serializer = ArticleSerializers(article, data=request.data)
        if article_serializer.is_valid():
            article_serializer.save()
            return Response(article_serializer.data)
        return Response(article_serializer.errors, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        article = self.get_object(id)
        article.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


# generic view rest_framework
class GenericAPIView(
    generics.GenericAPIView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
):
    serializer_class = ArticleSerializers
    queryset = Article.objects.all()
    lookup_field = "id"

    def get_object(self, id):
        try:
            Article.objects.get(id=id)
        except Article.DoesNotExist as err:
            raise err

    def get(self, request, id=None):
        if id:
            return self.retrieve(request)
        else:
            return self.list(request)

    def post(self, request):
        return self.create(request)

    def put(self, request, id=None):
        return self.update(request, id)

    def delete(self, request, id=None):
        if id:
            return self.delete(request, id)


# shortcut generic_view
# Article view 
class ListAllView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ArticleSerializers
    queryset = Article.objects.all()
    lookup_field = "id"
    paginate_by = 10


class RetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ArticleSerializers
    queryset = Article.objects.all()
    lookup_field = "id"

# Book view
class ListAllBookView(generics.ListCreateAPIView):
    permission_classes=(IsAuthenticated,)
    serializer_class = BookSerializers
    queryset = Book.objects.all()
    lookup_field = 'id'
    
    def get_object(self, id):
        try:
            return
        except Author.DoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    def post(self, request):
        book_serializer = BookSerializers(Book, request.data)
        print("------------------------------------------",book_serializer.is_valid())
        if book_serializer.is_valid():        
            book_serializer.save()
            return Response(book_serializer.data, status=status.HTTP_201_CREATED)
        return Response(book_serializer.errors, status=status.HTTP_201_CREATED)




        #     data= request.data
        #     author_obj = self.get_object(data['author_id'])
        #     instance = Book.objects.create(title= data['title'] , description=data['description'] , publisher=data['publisher'])
        #     instance.author.add(author_obj)
        #     instance.save()
        #     return Response("book_serializer.data", status=status.HTTP_201_CREATED)
        # return Response("book_serializer.error", status=status.HTTP_201_CREATED)

class RetrieveUpdateDestroyBookView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = BookSerializers
    queryset = Book.objects.all()
    lookup_field = "id"

# Author view
class ListAllAuthorView(generics.ListCreateAPIView , mixins.CreateModelMixin):
    permission_classes=(IsAuthenticated,)
    serializer_class = AuthorSerializers
    queryset = Author.objects.all()
    lookup_field = 'id'


class RetrieveUpdateDestroyAuthorView(generics.RetrieveUpdateDestroyAPIView , mixins.CreateModelMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = AuthorSerializers
    queryset = Author.objects.all()
    lookup_field = "id"

   

# orther view

class ListAllOtherView(generics.ListCreateAPIView):
    permission_classes=(IsAuthenticated,)
    serializer_class = BookSerializers
    queryset = Book.objects.all()
    lookup_field = 'id'

class RetrieveUpdateDestroyOtherView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = BookSerializers
    queryset = Book.objects.all()
    lookup_field = "id"

