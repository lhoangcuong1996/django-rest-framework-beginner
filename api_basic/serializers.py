from rest_framework import serializers

from .models import Article , Book , Author

# define class <ModelSerializer>
# inherit serializers class
# class ArticleSerializers(serializers.Serializer):
#     title = serializers.CharField(max_length=1000)
#     author = serializers.CharField(max_length=50)
#     email = serializers.EmailField(max_length=50)
#     date = serializers.DateField()
#
#     def create(self, validated_data):
#         return Article.objects.create(validated_data)
#
#     def update(self, instance , validated_data):
#         instance.title = validated_data.get('title', instance.title)
#         instance.author = validated_data.get('author', instance.author)
#         instance.email = validated_data.get('email', instance.email)
#         instance.date = validated_data.get('date', instance.date)
#         return instance

# same the definition above
class ArticleSerializers(serializers.ModelSerializer):
    # When use class meta, we indicate that the class serializer extend some model and how many field you need
    class Meta:
        model               = Article
        # fields = ['id', 'title', 'author']
        # if you want inherit all fields
        fields              = "__all__"



class AuthorSerializers(serializers.ModelSerializer):
    class Meta:
        ordering           = ['-id']
        fields             = "__all__"
        model              = Author
        extra_kwargs       = {'author': {'required': False}}

class BookSerializers(serializers.ModelSerializer):
    author = AuthorSerializers(many=True ,required=False)
    class Meta:
        ordering           = ['-id']
        model              = Book
        fields             = "__all__"
        extra_kwargs       = {'books': {'required': False}}

# class OrderSerializers(serializers.ModelSerializer):
#     books= BookSerializers(many=True ,required=False)
#     class Meta:
#         ordering           = ['-id']
#         fields             = "__all__"
#         model              = Order
#         extra_kwargs       = {'author': {'required': False}}

    





