from django.conf import settings
from rest_auth.registration.serializers import (
    SocialLoginSerializer as RestSocialLoginSerializer,
)


class SocialLoginSerializer(RestSocialLoginSerializer):
    def validate(self, attrs):

        # Overwrite settings to ignore validation rules of Unique Email if user has registered local account
        setattr(settings, "ACCOUNT_UNIQUE_EMAIL", False)
        return super(SocialLoginSerializer, self).validate(attrs)
